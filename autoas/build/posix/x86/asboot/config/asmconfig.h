#ifndef _AS_MCONF_H_

#ifndef USE_ECUM
#define USE_ECUM 1
#endif

#ifndef USE_SCHM
#define USE_SCHM 1
#endif

#ifndef USE_MCU
#define USE_MCU 1
#endif

#ifndef USE_DET
#define USE_DET 1
#endif

#ifndef USE_CLIB_ASHEAP
#define USE_CLIB_ASHEAP 1
#endif

#ifndef USE_CLIB_MISCLIB
#define USE_CLIB_MISCLIB 1
#endif

#ifndef USE_CLIB_STDIO_PRINTF
#define USE_CLIB_STDIO_PRINTF 1
#endif

#ifndef USE_CLIB_NEWLIB
#define USE_CLIB_NEWLIB 1
#endif

#ifndef USE_SHELL
#define USE_SHELL 1
#endif

#ifndef USE_CLIB_STRTOK_R
#define USE_CLIB_STRTOK_R 1
#endif

#ifndef USE_CLIB_MBOX
#define USE_CLIB_MBOX 1
#endif

#ifndef USE_CLIB_QSORT
#define USE_CLIB_QSORT 1
#endif

#ifndef USE_PCI
#define USE_PCI 1
#endif

#ifndef USE_CAN
#define USE_CAN 1
#endif

#ifndef USE_CANIF
#define USE_CANIF 1
#endif

#ifndef USE_PDUR
#define USE_PDUR 1
#endif

#ifndef USE_COM
#define USE_COM 1
#endif

#ifndef USE_COMM
#define USE_COMM 1
#endif

#ifndef USE_CANTP
#define USE_CANTP 1
#endif

#ifndef USE_CANNM
#define USE_CANNM 1
#endif

#ifndef USE_DCM
#define USE_DCM 1
#endif

#ifndef USE_CANNM
#define USE_CANNM 1
#endif

#ifndef USE_CANSM
#define USE_CANSM 1
#endif

#ifndef USE_NM
#define USE_NM 1
#endif

#ifndef USE_OSEKNM
#define USE_OSEKNM 1
#endif

#ifndef USE_XCP
#define USE_XCP 1
#endif

#ifndef USE_SOAD
#define USE_SOAD 1
#endif

#ifndef USE_DOIP
#define USE_DOIP 1
#endif

#ifndef USE_LWIP
#define USE_LWIP 1
#endif

#ifndef USE_FTP
#define USE_FTP 1
#endif

#ifndef USE_SD
#define USE_SD 1
#endif

#ifndef USE_ASKAR
#define USE_ASKAR 1
#endif

#ifndef USE_PTHREAD
#define USE_PTHREAD 1
#endif

#ifndef USE_PTHREAD_CLEANUP
#define USE_PTHREAD_CLEANUP 1
#endif

#ifndef USE_PTHREAD_SIGNAL
#define USE_PTHREAD_SIGNAL 1
#endif

#ifndef USE_FATFS
#define USE_FATFS 1
#endif

#ifndef USE_FATFS_DRV
#define USE_FATFS_DRV 1
#endif

#ifndef USE_LIBELF
#define USE_LIBELF 1
#endif

#ifndef USE_LIBDL
#define USE_LIBDL 1
#endif

#ifndef USE_VFS
#define USE_VFS 1
#endif

#ifndef USE_DEV
#define USE_DEV 1
#endif

#ifndef USE_COMMONXML
#define USE_COMMONXML 1
#endif

#if defined(USE_RTTHREAD) && defined(USE_ARCH_X86)
#define _EXFUN(N,P) N P
#endif
#ifndef configTOTAL_HEAP_SIZE
#define configTOTAL_HEAP_SIZE 0x800000
#endif
#ifndef PAGE_SIZE
#define PAGE_SIZE 0x1000
#endif
#ifndef OS_TICKS_PER_SECOND
#define OS_TICKS_PER_SECOND 1000
#endif
#ifndef USECONDS_PER_TICK
#define USECONDS_PER_TICK 10000
#endif
#ifndef ENABLE_SHELL_ECHO_BACK
#define ENABLE_SHELL_ECHO_BACK
#endif
#ifndef OS_STK_SIZE_SCALER
#define OS_STK_SIZE_SCALER 2
#endif
#ifndef FTPD_DEBUG
#define FTPD_DEBUG
#endif
#ifndef USE_NEWLIB
#define USE_NEWLIB
#endif
#ifndef __X86__
#define __X86__
#endif
#ifndef CAN_LL_DL
#define CAN_LL_DL 64
#endif
#ifndef _SYS__PTHREADTYPES_H_
#define _SYS__PTHREADTYPES_H_
#endif
#endif /* _AS_MCONF_H_ */
